$(function() {
	jQuery('img.svg').each(function(){
            var $img = jQuery(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');

            jQuery.get(imgURL, function(data) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');

                // Add replaced image ID to the new SVG
                if(typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml');

        });


	// var s = Snap(800, 600);
	// var cirlce = s.circle(150, 150, 100);
	// var cirlce = s.circle(150, 400, 100);

	// cirlce.attr({
	// 	fill: '#bada55',
	// 	stroke: '#000',
	// 	strokeWidth: 4,
	// });

	// cirlce.hover(function(){
	// 	cirlce.animate({
	// 		r: 30,
	// 		fill: '#da55c1',
	// 	}, 150);
	// }, function(){
	// 	cirlce.animate({
	// 		r: 100,
	// 		fill: '#bada55',
	// 	}, 150);
	// });

	// s.click(function(e){
	// 	if(e.target.tagName == 'circle')
	// 	{

	// 	} else {
	// 		var el = s.circle(e.offsetX, e.offsetY, 20).drag().drag(null, function(){
	// 			el.animate({ fill: '#bada55' }, 150);
	// 		}, function(){
	// 			el.animate({ fill: '#000' }, 150);
	// 		});
	// 	}
	// });

});
